package acad276.finalproject.stynt;

import android.content.SharedPreferences;
import android.content.Context;
import com.parse.ParseObject;
import com.parse.Parse;


/**
 * Created by Kimari, Riley, and Vincent on 12/6/15.
 */
public class Application extends android.app.Application {

    //runs debugging for location services, and google play services
    public static final boolean APPDEBUG = false;

    //The log tag during debugging
    public static final String APPTAG = "Stynt";

    //Saves the location in MainActivity and passes it along to the PostingActivity
    public static final String MAIN_PASS_POST = "location";

    //Potentially will be dropped, saves the radius of search preference
    public static final String SEARCH_DISTANCE_SAVE = "searchDistance";

    //First Initialied distance of search radius
    private static final float START_SEARCH_DISTANCE = 4000.0f;

    //Stynt Preferences
    private static SharedPreferences sharedPref;

    //Stynt Configuration
    private static Configuration configHelper;

    //Application Method
    public Application() {

    }

    @Override
    public void onCreate(){
        super.onCreate();

        //Stynt Parse Application ID and Client Key
        ParseObject.registerSubclass(StyntPost.class);
        Parse.initialize(this, "Guz5qCBUS52E449fUmZK2y64lp1P9DKCUQ842ocy",
                "kpRHU6uYKwPTryvzZekJxiXLqeYDVffAciyL8Bbt");

        sharedPref = getSharedPreferences("com.parse.Stynt", Context.MODE_PRIVATE);
        configHelper = new Configuration();
        configHelper.fetchConfigIfNeeded();
    }
        //accesor for getting the current search distance
        public static float getSearchDistance(){
            return sharedPref.getFloat(SEARCH_DISTANCE_SAVE, START_SEARCH_DISTANCE);
        }
        //accesor for configuration
        public static Configuration getConfiguration(){
            return configHelper;
        }

        //Mutator for Search Distance
        public static void setSearchDistance(float value) {
            sharedPref.edit().putFloat(SEARCH_DISTANCE_SAVE, value).commit();
        }
}

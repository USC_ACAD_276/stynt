package acad276.finalproject.stynt;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.view.View.OnClickListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;
import com.parse.SaveCallback;



/**
 * Created by Kimari, Riley, Vincent on 12/6/15.
 * Post
 */

public class PostingActivity extends Activity {
    //variables
    private EditText postText;
    private EditText priceText;
    private TextView characterCountText;
    private Button postButton;
    private int maxCharacterCount = Application.getConfiguration().getPostMaxCharacterCount();
    private ParseGeoPoint geoPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.posting_layout);

        //passes location of main to posting activity
        Intent intent = getIntent();
        Location location = intent.getParcelableExtra(Application.MAIN_PASS_POST);
        geoPoint = new ParseGeoPoint(location.getLatitude(), location.getLongitude());

        //Textchange listener
        postText = (EditText) findViewById(R.id.post_text);
        postText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                updatePostingButtonState();
                updateCharcterCountTextViewText();
            }
        });

        characterCountText = (TextView) findViewById(R.id.character_count_text);

        postButton = (Button) findViewById(R.id.post_button);
        postButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                posting();
            }
        });

        updatePostingButtonState();
        updateCharcterCountTextViewText();

    }


    private void posting () {
        String text = postText.getText().toString().trim();
        String number = priceText.getText().toString().trim();


//        Set up the post via ProgressDialog
        final ProgressDialog dialog = new ProgressDialog(PostingActivity.this);
        dialog.setMessage("Posting now!");
        dialog.show();


//        Create the post
        StyntPost posting = new StyntPost();


//        Set user's location to post
        posting.setLocation(geoPoint);
        posting.setText(text);
        posting.setUser(ParseUser.getCurrentUser());
        ParseACL acl = new ParseACL();


//        Set the read access to public
        acl.setPublicReadAccess(true);
        posting.setACL(acl);


//        Save user's post
        posting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

            }
        });
    }



    private String getPostingTextText () {
       return postText.getText().toString().trim();
    }

    private String getPricingTextText() {
        return priceText.getText().toString().trim();
    }

    private void updatePostingButtonState(){
        int length = getPostingTextText().length();
        boolean enabled = length > 0 && length < maxCharacterCount;
        postButton.setEnabled(enabled);
    }

    private void updateCharcterCountTextViewText(){
        String charcterCount = String.format("%d/%d", postText.length(), maxCharacterCount);
        characterCountText.setText(charcterCount);
    }
}

package acad276.finalproject.stynt;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import android.app.ProgressDialog;
import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Kimari, Riley, and Vincent on 12/5/15.
 * SignUp
 */

//this class will display at the login screen

public class RegistrationActivity extends Activity {
    //editable variables for user: Username and Password.

    private EditText usernameText;
    private EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);

        //The fun starts here! User registration

        usernameText = (EditText) findViewById(R.id.username);
        passwordText = (EditText) findViewById(R.id.password);

        //Submit button
        Button mActionButton = (Button) findViewById(R.id.action_button);
        mActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick (View view) {
                register();
            }
        });
    }

    private void register() {
        String username = usernameText.getText().toString().trim();
        String password = passwordText.getText().toString().trim();

        //Checks to make sure the registration data is filled out
        boolean validationError = false;
        StringBuilder validationErrorMessage = new StringBuilder("Don't forget to ");
        if (username.length() == 0) {
            validationError = true;
            validationErrorMessage.append("enter a username, yo!");
        }
        if (password.length() == 0) {
            if (validationError) {
                validationErrorMessage.append(" And ");
            }
            validationError = true;
            validationErrorMessage.append("enter a password, yo!");
        }
        validationErrorMessage.append("?!?");

        // If anything is wrong with the validation display the error
        if (validationError) {
            Toast.makeText(RegistrationActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
            return;
        }

        //Progress Message
        final ProgressDialog dialog = new ProgressDialog(RegistrationActivity.this);
        dialog.setMessage("Registering!");
        dialog.show();

        //New Parse User
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);

        //Parse register method
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                dialog.dismiss();
                if (e != null) {
                    //Error Message
                    Toast.makeText(RegistrationActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    //Dispatch Activity
                    Intent intent = new Intent(RegistrationActivity.this, RunTimeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
    }
}

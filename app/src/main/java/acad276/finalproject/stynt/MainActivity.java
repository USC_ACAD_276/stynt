package acad276.finalproject.stynt;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import android.app.*;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends FragmentActivity implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    //Location Paramaters
    //Milliseconds
    private static final int MILLISECONDS_SECOND = 1000;

    //Update interval is 10 milliseconds
    private static final int UPDATE_INTERVAL_SECONDS = 5;

    //Interval Ceiling
    private static final int CEILING_SECONDS = 1;

    //updates interval with milliseconds
    private static final long UPDATE_INTERVAL_MILLISECONDS = MILLISECONDS_SECOND * UPDATE_INTERVAL_SECONDS;

    //Ceiling update intervals
    private static final long CEILING_MILISECONDS = MILLISECONDS_SECOND * CEILING_SECONDS;

    //Unit Conversions

    //feet to meters
    private static final float METERS_FEET = 0.3048f;

    //kilometers to meters
    private static final int METERS_KILOMETER = 1000;

    //Calculating Map Bounds
    private static final double CALCULATION_DIFF = 1.0;

    //Calculating map bounds accuracy
    private static final float CALCULATION_ACCURACY = 0.01f;

    //Max results for Parse Query
    private static final int MAX_SEARCH_RES = 20;

    //Max post radius for map in km
    private static final int MAX_SEARCH_DIST = 100;

    //Map Fragment
    private SupportMapFragment mapFragment;

    //Circle around map
    private Circle mapCirc;

    //Map Radius in feet
    private float radi;
    private float lastRadi;

    //Helps with changes in location and map
    private final Map<String, Marker> mapMarkers = new HashMap<String, Marker>();
    private int mostRecentMapUpdate;
    private boolean hasSetUpInitialLocation;
    private String selectedPostObjectId;
    private Location lastLoc;
    private Location currentLoc;

    //Requests to connect to Location Services
    private LocationRequest locationRequest;

    //Stores location into this google client
    private GoogleApiClient locationClient;

    //Adapter for Parse
    private ParseQueryAdapter<StyntPost> postsQueryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        radi = Application.getSearchDistance();
        lastRadi = radi;
        setContentView(R.layout.activity_main);

        //Location request
        locationRequest = LocationRequest.create();

        //Sets update interval
        locationRequest.setInterval(UPDATE_INTERVAL_MILLISECONDS);

        //High Accuracy
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //Ceiling set to one minute
        locationRequest.setFastestInterval(CEILING_MILISECONDS);

        //Creates new location client
        locationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //Customized Query
        ParseQueryAdapter.QueryFactory<StyntPost> factory =
                new ParseQueryAdapter.QueryFactory<StyntPost>() {
                    public ParseQuery<StyntPost> create() {
                        Location newLoc = (currentLoc == null) ? lastLoc : currentLoc;
                        ParseQuery<StyntPost> query = StyntPost.getQuery();
                        query.include("user");
                        query.orderByDescending("createdAt");
                        query.whereWithinKilometers("location", geoPointFromLocation(newLoc), radi * METERS_FEET / METERS_KILOMETER);
                        query.setLimit(MAX_SEARCH_RES);
                        return query;
                    }
                };

        //Query adapter
        postsQueryAdapter = new ParseQueryAdapter<StyntPost>(this, factory) {
            @Override
            public View getItemView(StyntPost post, View view, ViewGroup parent) {
                if (view == null) {
                    view = View.inflate(getContext(), R.layout.stynt_post, null);
                }
                TextView contentView = (TextView) view.findViewById(R.id.content);
                TextView usernameView = (TextView) view.findViewById(R.id.username);
                contentView.setText(post.getText());
                usernameView.setText(post.getUser().getUsername());
                return view;
            }
        };

        //Disable loading when on a view
        postsQueryAdapter.setAutoload(false);

        //Disables pagination
        postsQueryAdapter.setPaginationEnabled(false);

        //Puts a query adapter to the view
        ListView postsListView = (ListView) findViewById(R.id.posts_listview);
        postsListView.setAdapter(postsQueryAdapter);

        //Set Up handler for item Selection
        postsListView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final StyntPost item = postsQueryAdapter.getItem(position);
                selectedPostObjectId = item.getObjectId();
                mapFragment.getMap().animateCamera(
                        CameraUpdateFactory.newLatLng(new LatLng(item.getLocation().getLatitude(), item.getLocation().getLongitude())),
                        new CancelableCallback() {
                            public void onFinish() {
                                Marker mark = mapMarkers.get(item.getObjectId());
                                if (mark != null) {
                                    mark.showInfoWindow();
                                }
                            }

                            public void onCancel() {
                            }
                        });
                Marker marker = mapMarkers.get(item.getObjectId());
                if (marker != null) {
                    marker.showInfoWindow();
                }
            }
        });

        //Map Fragments
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);

        //Current location is represented by a blue dot
        mapFragment.getMap().setMyLocationEnabled(true);
        //Camera change handler
        mapFragment.getMap().setOnCameraChangeListener(new OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                //if the camera changes, it updates
                doMapQuery();
            }
        });

        //Post Button Click Handler
        Button postButton = (Button) findViewById(R.id.post_button);
        postButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //With location info, allow posts
                Location myLoc = (currentLoc == null) ? lastLoc : currentLoc;
                if (myLoc == null) {
                    Toast.makeText(MainActivity.this,
                            "Try again once your location is showing on the map", Toast.LENGTH_LONG).show();
                    return;
                }

                Intent intent = new Intent(MainActivity.this, PostingActivity.class);
                intent.putExtra(Application.MAIN_PASS_POST, myLoc);
                startActivity(intent);

            }
        });


    }


//When activity is not available this is called. It stops updating & disconnects.

    @Override
    public void onStop() {
//        if user is connected...
        if (locationClient.isConnected()) {
            stopPeriodicUpdates();
        }


//        Once disconnect() is called the user is done
        locationClient.disconnect();
        super.onStop();
    }


    //        When Activity is restarted it's called and location services are called
    @Override
    protected void onStart() {
        super.onStart();
        locationClient.connect();
    }


//    Updates the view, called when the Activity is resumed
    @Override
            protected void onResume() {
        super.onResume();

        Application.getConfiguration().fetchConfigIfNeeded();

//        Fetch most recent distance preference
        radi = Application.getSearchDistance();

//      Look at previous location and show available data
        if (lastLoc != null) {
            LatLng myLatLng = new LatLng(lastLoc.getLatitude(), lastLoc.getLongitude());

//      If map radius preferences have been changed, update it
        if (lastRadi != radi) {
            updateZoom(myLatLng);
        }

//      Update the maps radius
        updateCircle(myLatLng);
        }
//      Implement/save new map radius
        lastRadi = radi;

//      maps query to update the view with most recent data
        doMapQuery();
        doListQuery();
    }

    //Setting up Debugging to help log errors and connectivity issues when we get an error message
    //this way we can see which services are not working

    protected void onActivityResult(int requestCode, Intent intent) {
        //switch statements based on request
        switch (requestCode) {
            //if the request code is onConnectionFailed
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:

                switch (requestCode) {
                    //if google play services resolved problem
                    case Activity.RESULT_OK:
                        if(Application.APPDEBUG){
                            //logs result
                            Log.d(Application.APPTAG, "The App Connected to Google Play services succesfully");
                        }
                        break;

                    //if a different result was returned by Google Play
                    default:
                        if(Application.APPDEBUG) {
                            //logs result
                            Log.d(Application.APPTAG, "The App could not connect to Google Play services");
                        }
                        break;
                }

                //if a different request was sent back
                default:
                    if(Application.APPDEBUG){
                        //recieved an unknown requestCode
                        Log.d(Application.APPTAG, "Unknown Request Code");
                    }
                    break;
        }
    }

    //Makes sure that Google PLay Services is up and running before making request to server
    private boolean servicesConnected(){
        //Is Google play running?
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        //if Google Play services is live
        if(ConnectionResult.SUCCESS == resultCode) {
            if(Application.APPDEBUG) {
                //Logs that Google Play is availble
                Log.d(Application.APPTAG, "Google Play is Available");
            }
            return true;
            //if its not available
        } else {
            //give error message
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0);
            if(dialog != null) {
                ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
                errorDialogFragment.setDialog(dialog);
                errorDialogFragment.show(getSupportFragmentManager(), Application.APPTAG);
            }
            return false;
        }
    }

    //Location Services Request, if it works we can start updating our current and future locations
    public void onConnected(Bundle bundle) {
        if (Application.APPDEBUG) {
            //connected to location services
            Log.d("Location Services Connected", Application.APPTAG);
        }
    }

    @Override
    public void onConnectionSuspended(int i){
        Log.i(Application.APPTAG, "GoogleApiClient connection is suspended");
    }
    //if location services fail it is called
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Google play will resovle some errors. If it has a resuloution we send an Intent that
        //Google play will try to use to solve error.
        if(connectionResult.hasResolution()) {
            try{
                //Starts activity to try to resolve error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                if(Application.APPDEBUG) {
                    //Will give messeage if Google Services canceld original PendIntent
                    Log.d(Application.APPTAG, "An error occured when connected to location services", e);
                }
            }
        } else {
            //If no resolution, display dialog with the error.
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    //Gives Location Updates to UI.
    public void onLocationChanged(Location location){
        currentLoc = location;
        if(lastLoc != null && geoPointFromLocation(location)
                .distanceInKilometersTo(geoPointFromLocation(lastLoc)) < 0.01){
            //if user loc has not changed more than 10 meters no change
            return;
        }
        lastLoc = location;
        LatLng myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (!hasSetUpInitialLocation) {
            //Zooms to current Location
            updateZoom(myLatLng);
            hasSetUpInitialLocation = true;
        }
        //Updates Map
        updateCircle(myLatLng);
        doMapQuery();
        doListQuery();
    }

    //when request to start update, sends request to location services
    private  void startPeriodicUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                locationClient, locationRequest, this);
    }

    //when request stops updating, request location services
    private void stopPeriodicUpdates() {
        locationClient.disconnect();
    }

    //Gets your current location
    private Location getLocation() {
        //If Google Play is availble
        if(servicesConnected()) {
            //Get current locaiton
            return LocationServices.FusedLocationApi.getLastLocation(locationClient);
        }else {
            return null;
        }
    }
    //Query update for list view
    private void doListQuery() {
        Location myLoc = (currentLoc == null) ? lastLoc : currentLoc;
        //if location info is availble, loads locaiton info
        if(myLoc != null) {
            //Refreshes list view with new data
            postsQueryAdapter.loadObjects();
        }
    }

    //Query update for map view
    private void doMapQuery() {
        final int myUpdateNumber = ++mostRecentMapUpdate;
        Location myLoc = (currentLoc == null) ? lastLoc : currentLoc;
        //location info isnt avalable, cleans markers
        if(myLoc == null) {
            cleanUpMarkers(new HashSet<StyntPost>());
            return;
        }
        final ParseGeoPoint myPoint = geoPointFromLocation(myLoc);
        //Makes map pare query
        final ParseQuery<StyntPost> mapQuery = StyntPost.getQuery();
        //makes more query filters
        mapQuery.whereWithinKilometers("location", myPoint, MAX_SEARCH_DIST);
        mapQuery.include("user");
        mapQuery.orderByDescending("createdAt");
        mapQuery.setLimit(MAX_SEARCH_RES);
        //Query runs in background
        mapQuery.findInBackground(new FindCallback<StyntPost>() {
            @Override
            public void done(List<StyntPost> objects, ParseException e) {
                if (e != null) {
                    if(Application.APPDEBUG) {
                        Log.d(Application.APPTAG, "An error occured while making query for map post.", e);
                    }
                    return;
                }
                //Make sure results are most recent and fresh in case of duplicates
                if(myUpdateNumber != mostRecentMapUpdate){
                    return;
                }
                //Posts Show on the map
                Set<String> toKeep = new HashSet<String>();
                //loops through search results
                for(StyntPost post :objects) {
                    //adds post to other map pins
                    toKeep.add(post.getObjectId());
                    //Checks for existing marker
                    Marker oldMarker = mapMarkers.get(post.getObjectId());
                    //Sets map markers location
                    MarkerOptions markerOps =
                            new MarkerOptions().position(new LatLng(post.getLocation().getLatitude(),
                                    post.getLocation().getLongitude()));
                    //Sets up marker properties based on search radius
                    if(post.getLocation().distanceInKilometersTo(myPoint) > radi * METERS_FEET / METERS_KILOMETER) {
                        //Checks existing out of range marker
                        if(oldMarker != null) {
                            if(oldMarker.getSnippet() == null) {
                                //Out of range marker exists, no need to add
                                continue;
                            }else {
                                //Marker is out of range and needs to be refreshed
                                oldMarker.remove();
                            }
                        }
                        //Shows red marker with a defined title but no snippet
                        markerOps = markerOps.title("Post is out of Range").icon(
                                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    } else {
                        //Looks for existing range marker
                        if (oldMarker != null) {
                            if(oldMarker.getSnippet() != null) {
                                //in range marker exists, does not add
                                continue;
                            }else {
                                oldMarker.remove();
                            }
                        }
                        //Makes a blue marker to show post
                        markerOps = markerOps.title(post.getText()).snippet(post.getUser().getUsername())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                    }
                    //Adds a new marker
                    Marker marker = mapFragment.getMap().addMarker(markerOps);
                    mapMarkers.put(post.getObjectId(), marker);
                    if (post.getObjectId().equals(selectedPostObjectId)){
                        marker.showInfoWindow();
                        selectedPostObjectId = null;
                    }
                }
                //Cleans old markers
                cleanUpMarkers(toKeep);
            }
        });
    }

    //Method to clean markers
    private void cleanUpMarkers(Set<StyntPost> markerToKeep) {
        for (String objId: new HashSet<String>(mapMarkers.keySet())) {
            if(!markerToKeep.contains(objId)) {
                Marker marker = mapMarkers.get(objId);
                marker.remove();
                mapMarkers.get(objId).remove();
                mapMarkers.remove(objId);
            }
        }
    }
    //Method to get Parse Geo point for locaiton
    private ParseGeoPoint geoPointFromLocation(Location loc) {
        return new ParseGeoPoint(loc.getLatitude(), loc.getLongitude());
    }

    //Makes a Circle on the map showing the search radius
    private void updateCircle(LatLng myLatLng) {
        if(mapCirc == null) {
            mapCirc = mapFragment.getMap().addCircle(new CircleOptions().center(myLatLng).radius(radi * METERS_FEET));
            int baseColor = Color.DKGRAY;
            mapCirc.setStrokeColor(baseColor);
            mapCirc.setStrokeWidth(3);
            mapCirc.setFillColor(Color.argb(50, Color.red(baseColor), Color.green(baseColor),
                    Color.blue(baseColor)));
        }
        mapCirc.setCenter(myLatLng);
        //makes radi feet instead of meters
        mapCirc.setRadius(radi * METERS_FEET);
    }

    //Zooms map to show new search radius area
    private void updateZoom (LatLng myLatLng) {
        //bounds of zoom to
        LatLngBounds bounds = calculateBoundsWithCenter(myLatLng);
        //Zooms to bounds
        mapFragment.getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
    }

    //Calculates offset bounds
    private double calculateLatLngOffset(LatLng myLatLng, boolean bLatOff) {
        //returns offset
        double latLngOff = CALCULATION_DIFF;
        //makes desired offset distance
        float desiredOffMeters = radi * METERS_FEET;
        //distance calculation
        float[] distance = new float[1];
        boolean foundMax = false;
        double foundMinDiff = 0;
        //loops through to find offset
        do{
            //Calculated distance between interest and offset
            if(bLatOff) {
                Location.distanceBetween(myLatLng.latitude, myLatLng.longitude, myLatLng.latitude
                        + latLngOff, myLatLng.longitude, distance);
            }else {
                Location.distanceBetween(myLatLng.latitude, myLatLng.longitude, myLatLng.latitude,
                        myLatLng.longitude + latLngOff, distance);
            }
            //compares current difference with wanted difference
            float distanceDiff = distance[0] - desiredOffMeters;
            if (distanceDiff < 0) {
                //Need to set it to desired
                if(!foundMax) {
                    foundMinDiff = latLngOff;
                    //Increases offset
                    latLngOff *= 2;
                }else {
                    double tmp = latLngOff;
                    //Increases calculated offset, but slower
                    latLngOff += (latLngOff - foundMinDiff) / 2;
                    foundMinDiff = tmp;
                    }
                }else {
                //if overshot decreases caclulated offset
                latLngOff -= (latLngOff - foundMinDiff) / 2;
                foundMax = true;
            }
        } while (Math.abs(distance[0] - desiredOffMeters) > CALCULATION_ACCURACY);
        return latLngOff;
    }

    //Calculates bounds while map zooming
    LatLngBounds calculateBoundsWithCenter(LatLng myLatLng) {
        //makes a bounds
        LatLngBounds.Builder builder = LatLngBounds.builder();

        //Calculates east to west points in the bounds
        double longDiff = calculateLatLngOffset(myLatLng, false);
        LatLng east = new LatLng(myLatLng.latitude, myLatLng.longitude + longDiff);
        builder.include(east);
        LatLng west = new LatLng(myLatLng.latitude, myLatLng.longitude - longDiff);
        builder.include(west);

        //Now for North and South
        double latDiff = calculateLatLngOffset(myLatLng, true);
        LatLng north = new LatLng(myLatLng.latitude + latDiff, myLatLng.longitude);
        builder.include(north);
        LatLng south = new LatLng(myLatLng.latitude - latDiff, myLatLng.longitude);
        builder.include(south);

        return builder.build();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Makes the menu and adds items to present
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menu.findItem(R.id.action_settings).setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(MainActivity.this, Settings.class));
                return true;
            }
            });
        return true;
    }
    //Dialog returned by Google PLay for conneciton error
    private void showErrorDialog(int errorCode) {
        //gets error dialog from Google
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode, this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

        //If Google Play can provide an error
        if (errorDialog != null) {

            // Creates a new DialogFragment to show the error
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Sets the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getSupportFragmentManager(), Application.APPTAG);
        }
    }
    //Defines Dialog Fragment
    public static class ErrorDialogFragment extends DialogFragment {
        // field to contain error dialog
        private Dialog mDialog;

        //default constructor
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

       //sets dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        //method returns a dialog to dialog fragment
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
}

package acad276.finalproject.stynt;

import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseObject;
import com.parse.ParseClassName;


/**
 * Created by Kimari, Riley, and Vincent on 12/6/15.
 */

@ParseClassName("Stynt Posts")
public class StyntPost extends ParseObject {

    //accessors and mutators for text, user, and location


    public String getText() {
        return getString("text");
    }

    public void setText(String value) {
        put("text", value);
    }

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser value){
        put("user", value);
    }

    public ParseGeoPoint getLocation(){
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint value) {
        put("location", value);
    }

    public static ParseQuery<StyntPost> getQuery() {
        return ParseQuery.getQuery(StyntPost.class);
    }
}

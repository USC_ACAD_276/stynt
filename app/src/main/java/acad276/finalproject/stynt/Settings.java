package acad276.finalproject.stynt;

import java.util.List;
import java.util.Collections;
import com.parse.ParseUser;
import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

/**
 * Created by Kimari, Riley, Vincent on 12/6/15.
 */
public class Settings extends Activity {

    private List<Float> availbleOptions = Application.getConfiguration().getSearchDistanceAvailableOptions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        //gets current search distance
        float currentSearchDist = Application.getSearchDistance();
        if(!availbleOptions.contains(currentSearchDist)) {
            availbleOptions.add(currentSearchDist);
        }
        Collections.sort(availbleOptions);

        //different choices for distance
        RadioGroup searchDistanceRadio = (RadioGroup) findViewById(R.id.search_distance_radio);

        //
        for(int index = 0; index < availbleOptions.size(); index++){
            float searchDistance = availbleOptions.get(index);

            RadioButton button = new RadioButton(this);
            button.setId(index);
            button.setText(getString(R.string.settings_distance_format, (int) searchDistance));
            searchDistanceRadio.addView(button, index);

            if(currentSearchDist == searchDistance){
                searchDistanceRadio.check(index);
            }
        }
        //the chosen selection is saved as the application
        searchDistanceRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Application.setSearchDistance(availbleOptions.get(checkedId));
            }
        });

        //allows the user to logout of the app
        Button logoutButton = (Button) findViewById(R.id.logout);
        logoutButton.setOnClickListener(new OnClickListener(){
            public void onClick(View v) {
                ParseUser.logOut();
                Intent intent = new Intent(Settings.this, RunTimeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }
}

package acad276.finalproject.stynt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseUser;

/**
 * Created by Kimari, Riley, and Vincent on 12/5/15.
 * Dispatch
 */

public class RunTimeActivity extends Activity{

    public RunTimeActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Checks if there is user info or not
        if (ParseUser.getCurrentUser() != null) {
            //Intent for logged in
            startActivity(new Intent(this, MainActivity.class));
        }else{
            //Intent for logged out
            startActivity(new Intent(this, GreetingsActivity.class));
        }
    }
}

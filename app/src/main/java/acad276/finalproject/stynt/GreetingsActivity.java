package acad276.finalproject.stynt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


/**
 * Created by Kimari, Riley, and Vincent on 12/5/15.
 * Welcome
 */
public class GreetingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greetings);

        //Login button listener
        Button loginButton = (Button) findViewById(R.id.action_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Login Intent
                startActivity(new Intent(GreetingsActivity.this, LoginActivity.class));
            }
        });

        //Register button listener
        Button registerButton = (Button) findViewById(R.id.action_button);
        registerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Register Intent
                startActivity(new Intent(GreetingsActivity.this, RegistrationActivity.class));
            }
        });
    }
}

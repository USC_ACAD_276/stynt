package acad276.finalproject.stynt;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;



/**
 * Created by Kimari, Riley, Vincent on 12/5/15.
 */

public class LoginActivity extends Activity{
    //Username and Password
    private EditText usernameText;
    private EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        //Login activity
        usernameText = (EditText) findViewById(R.id.username);
        passwordText = (EditText) findViewById(R.id.password);
        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == R.id.edittext_action_login || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    login();
                    return true;
                }
                return false;
            }
        });

        //Submit Listener
        Button actionButton = (Button) findViewById(R.id.action_button);
        actionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                login();
            }
        });
    }

    private void login() {
        String username = usernameText.getText().toString().trim();
        String password = passwordText.getText().toString().trim();

        //Checks login data to make sure it's filled out correctly
        boolean validationError = false;
        StringBuilder validationErrorMessage = new StringBuilder("Don't forget to ");
        if(username.length()==0){
            validationError=true;
            validationErrorMessage.append("enter a username, yo!");
        }
        if(password.length() == 0) {
            if(validationError){
                validationErrorMessage.append(" and ");
            }
            validationError = true;
            validationErrorMessage.append("enter a password, yo!");
        }
        validationErrorMessage.append("?!?");

        //Displays the error if there is one
        if(validationError) {
            Toast.makeText(LoginActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG).show();
            return;
        }

        //Progress dialog
        final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Logging In!");
        dialog.show();

        //Parse
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                dialog.dismiss();
                if(e != null) {
                    //Shows error message
                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }else {
                    //Runtime Intent
                    Intent intent = new Intent(LoginActivity.this, RunTimeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
    }
}

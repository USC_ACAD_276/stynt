package acad276.finalproject.stynt;

import android.app.*;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import com.parse.ParseException;
import com.parse.ParseConfig;
import com.parse.ConfigCallback;

/**
 * Created by Kimari, Riley, Vincent on 12/9/15.
 */


public class Configuration{
    private ParseConfig config;
    private long lastFetchedTime;


    public void fetchConfigIfNeeded() {
        //3600 seconds is equivalent to one hour
        final long refreshInterval = 3600;

        //if config is null or Current Time(- last fetched time) is less than refresh time, config equals current configuration, else last fetch time is 0
        if (config == null || System.currentTimeMillis() - lastFetchedTime > refreshInterval) {
            config = ParseConfig.getCurrentConfig();

            //Sets current time.
            ParseConfig.getInBackground(new ConfigCallback() {
                @Override
                public void done(ParseConfig parseConfig, ParseException e) {
                    if (e == null) {
                        //fetched the configuration succesfully
                        config = parseConfig;
                        lastFetchedTime = System.currentTimeMillis();
                    }else{
                        //did not fetch the correct time
                        lastFetchedTime = 0;
                    }
                }
            });
        }
    }

    //Options for Availble Search Options
    public List<Float> getSearchDistanceAvailableOptions() {
        //Array of distances
        final List<Float> defaultDistances = Arrays.asList(3500.0f, 4000.0f, 4500.0f, 5000.0f);

        //Array of numbers
        List<Number> options = config.getList("availableFilterDistances");
        if(options == null) {
            return defaultDistances;
        }

        //Additional typed search distances
        List <Float> typedOptions = new ArrayList<Float>();
        for (Number option : options) {
            typedOptions.add(option.floatValue());
        }

        return typedOptions;
    }

    public int getPostMaxCharacterCount(){
        int value = config.getInt("postMaxCharacterCount", 150);
        return value;
    }
}
